FROM jwilder/nginx-proxy:alpine  AS develop

RUN apk add --update --no-cache  tzdata supervisor dcron postfix ca-certificates tzdata curl openssl openssh-client libltdl shadow util-linux pciutils   usbutils   coreutils  binutils    findutils  grep  bash bash-completion gettext && \
    cp /usr/share/zoneinfo/Brazil/East /etc/localtime && \
     mkdir -p /run/supervisord/ && \
#Instalar o teste de mandar erro do shell para o sentry
    curl -sL https://sentry.io/get-cli/ | bash && \
    rm  -rf /tmp/* /var/cache/apk/*
#    && \
#    echo "* * * * *   sentry-send-logfile.sh" | crontab -


COPY conf/alpine /

RUN chmod +x /usr/local/bin/*.sh

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]

FROM develop AS production

FROM develop AS build