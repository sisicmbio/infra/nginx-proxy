#!/usr/bin/env bash

usage()
{
    echo "========================================================================================================"
    echo "Selecione em qual ambiente ira instalar:                           "
    echo ""
    echo "      -dev   | dev                 Ambiente DEV - Ambiente local do Desenvolvedor           "
    echo "      -test  | test                Ambiente TEST - Ambiente para execução dos testes automatizados."
    echo "========================================================================================================"
}

case $1 in
    -dev| dev )
        export PDCI_FILE_ENV_DOCKER=./.env.docker.dev
        export PDCI_PROJECT_NAME=dev
    ;;
    -test | test )
        export PDCI_FILE_ENV_DOCKER=./.env.docker.test
        export PDCI_PROJECT_NAME=dev_test
    ;;
    -h | --help )
        usage
        exit
    ;;
    * )
        usage
        exit 1
esac


echo
echo
echo "Informe seu login e senha para o registry.gitlab.com:"
echo
read -p 'Username: ' PDCI_CI_REGISTRY_USER
read -sp 'Password: ' PDCI_CI_REGISTRY_PASSWORD
echo
echo "Realizando login com  ${PDCI_CI_REGISTRY_USER} em registry.gitlab.com"
echo
docker logout

#echo "${PDCI_CI_REGISTRY_PASSWORD}" | docker   login --username ${PDCI_CI_REGISTRY_USER} --password-stdin  registry.gitlab.com


rm -rf ./.env


####################################################################
# RESTAURA OS SCHEMAS PARA O ESTADO INICIAL ANTES DOS TESTES
####################################################################
echo ""
echo ""
echo "=============================================================================================="
echo "  Restaura os esquemas em "
echo "        registry.gitlab.com/sisicmbio/sentry/image_file_schema_dump-dev:latest"
echo "=============================================================================================="
echo ""
echo ""

echo "version: '3.7'
networks:
  sentry_networks_test:
services:
  sentry_ifd_schema:
    mem_limit: 512m
    env_file:
    - ${PDCI_FILE_ENV_DOCKER}
    image: registry.gitlab.com/sisicmbio/sentry/image_file_schema_dump-dev:latest
    environment:
    - PDCI_BD_HOST=sentry_db
    networks:
    - sentry_networks_test
    command:
    - pdci_restaurar_schema_db.sh"> dev__image_file_schema_dump_dev_sentry.yml

echo ""
echo ""
echo "Iniciando restauracao da image-dump-file customizado para o projeto sentry"
echo ""
echo ""
echo ""
#docker   volume create ${PDCI_PROJECT_NAME}_session_php || true

#PDCI_PROJECT_NAME=dev_teste

if [   "${PDCI_PROJECT_NAME}" == "dev_teste" ]; then
    docker-compose -p ${PDCI_PROJECT_NAME:-dev} -f dev__image_file_schema_dump_dev_sentry.yml config
    docker-compose -p ${PDCI_PROJECT_NAME:-dev} -f dev__image_file_schema_dump_dev_sentry.yml pull
    docker-compose -p ${PDCI_PROJECT_NAME:-dev} -f dev__image_file_schema_dump_dev_sentry.yml rm -f -s sentry_ifd_schema
    docker  volume prune -f
    #docker  volume create dev_teste_session_php || true
    docker-compose -p ${PDCI_PROJECT_NAME:-dev} -f dev__image_file_schema_dump_dev_sentry.yml up -d
    docker-compose -p ${PDCI_PROJECT_NAME:-dev} -f dev__image_file_schema_dump_dev_sentry.yml logs -f  sentry_ifd_schema
    rm -rf dev__image_file_schema_dump_dev_sentry.yml

fi

#####################################################################
## SOBE OS SERVICOS DEPENDENTES DE APP
#####################################################################
#
## LER AS VARIAVEIS DO arquivo ${PDCI_FILE_ENV_DOCKER} QUE possui o VIRTUAL_HOST PARA QUE POSSAM SER LIDAS NO DOCKER-COMPOSER
##convercao de CRLF para LF =>  sed 's/\x0D$//'  ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//'  |  sed '/^$/d'
##ignorar todas as linhas comentadas => grep -v '^#'
##Deletar todas as linhas em branco =>  sed '/^$/d'

export $(sed 's/\x0D$//'  ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep '_VIRTUAL_HOST' | xargs)

export $(sed 's/\x0D$//'  ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep '_REGISTRY' | xargs)


#export pgadmin_random_port=$(_random=$((20034 % 100)) &&  printf "%02d" ${_random})
docker-compose -p ${PDCI_PROJECT_NAME:-dev} -f dev__requirement_docker-compose.yml config
docker-compose -p ${PDCI_PROJECT_NAME:-dev} -f dev__requirement_docker-compose.yml pull
#echo ""
#echo "Removendo container de teste caso esteja rodando ..."
#echo ""
docker-compose -p ${PDCI_PROJECT_NAME:-dev} -f dev__requirement_docker-compose.yml rm -f
docker  volume prune -f
docker-compose -p ${PDCI_PROJECT_NAME:-dev} -f dev__requirement_docker-compose.yml up -d
#docker-compose -p ${PDCI_PROJECT_NAME:-dev} -f dev__requirement_docker-compose.yml logs -f sentry_sicae_sql
docker-compose -p ${PDCI_PROJECT_NAME:-dev} -f dev__requirement_docker-compose.yml ps

unset $(sed 's/\x0D$//'  ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep '_VIRTUAL_HOST' | sed -E 's/(.*)=.*/\1/' | xargs)

unset $(sed 's/\x0D$//'  ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep '_REGISTRY' | sed -E 's/(.*)=.*/\1/' | xargs)
#
#
###################################################################################
### DEPLOY DE SERVICO DE APPLICACAO E BANCO DE DADOS CONFORME BRANCH OF DEVELOP
###################################################################################
##
## LER AS VARIAVEIS DO arquivo ${PDCI_FILE_ENV_DOCKER} QUE possui o VIRTUAL_HOST PARA QUE POSSAM SER LIDAS NO DOCKER-COMPOSER
##convercao de CRLF para LF =>  sed 's/\x0D$//'  ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//'  |  sed '/^$/d'
##ignorar todas as linhas comentadas => grep -v '^#'
##Deletar todas as linhas em branco =>  sed '/^$/d'
#
export $(sed 's/\x0D$//'  ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep '_VIRTUAL_HOST' | xargs)

export $(sed 's/\x0D$//'  ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep '_REGISTRY' | xargs)

export $(sed 's/\x0D$//'  ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep 'PDCI_' | xargs)

docker-compose -p ${PDCI_PROJECT_NAME:-dev} -f dev__docker-compose.yml config
docker-compose -p ${PDCI_PROJECT_NAME:-dev} -f dev__docker-compose.yml pull
docker-compose -p ${PDCI_PROJECT_NAME:-dev} -f dev__docker-compose.yml rm -f -s -v

#docker  volume rm  ${PDCI_PROJECT_NAME:-dev}_sentry_DocumentRoot -f

docker-compose -p ${PDCI_PROJECT_NAME:-dev} -f dev__docker-compose.yml up -d
#docker-compose -p ${PDCI_PROJECT_NAME:-dev} -f dev__docker-compose.yml logs -f sentry_sql
docker-compose -p ${PDCI_PROJECT_NAME:-dev} -f dev__docker-compose.yml ps

unset $(sed 's/\x0D$//'  ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep '_VIRTUAL_HOST' | sed -E 's/(.*)=.*/\1/' | xargs)

unset $(sed 's/\x0D$//'  ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep '_REGISTRY' | sed -E 's/(.*)=.*/\1/' | xargs)

unset $(sed 's/\x0D$//'  ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep 'PDCI_' | sed -E 's/(.*)=.*/\1/' | xargs)


#echo ""
#echo "*************************************************************************************"
#echo ""
#echo " Finalizado o deploy no DEV."
#echo ""
#echo ""
#echo "    Importante: "
#echo "     Adicione os dominios dos  "
#echo
#echo "         $(sed 's/\x0D$//'  ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep '_VIRTUAL_HOST' | xargs)  "
