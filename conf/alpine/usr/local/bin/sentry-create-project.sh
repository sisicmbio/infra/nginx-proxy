#!/usr/bin/env bash
#SENTRY_URL=${SENTRY_URL:-http://sentry.dev.pdci/}

#export SENTRY_URL=https://tctisentry.icmbio.gov.br/
#export SENTRY_API_KEY=634a87dbb4454f419499d1a5032c3965
#export SENTRY_AUTH_TOKEN=616cbac216f3463bbb5437ae310c325740187e1d33404e06b2059b2f63369af4
#export SENTRY_ORG=sentry
#export SENTRY_PROJECT=nginx-proxy
#SENTRY_AUTH_TOKEN
#
#The authentication token to use for all communication with Sentry.
#
#SENTRY_API_KEY=864ad189dd7c4c51ab0ac9ec6e176c53736eab57eabb4370bab94cbda0035b74
#The legacy API key for authentication if you have one.
#
#SENTRY_DSN (auth.dsn):
#The DSN to use to connect to sentry.
#
#SENTRY_URL (defaults.url):
#The URL to use to connect to sentry. This defaults to https://sentry.io/.
#
#SENTRY_ORG (defaults.org):
#The slug of the organization to use for a command.
#
#SENTRY_PROJECT (defaults.project):
#The slug of the project to use for a command.

sentry-cli --url ${SENTRY_URL} --auth-token ${SENTRY_AUTH_TOKEN} login