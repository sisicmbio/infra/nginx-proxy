#!/usr/bin/env bash
#PDCI_GIT_BRANCH=$(echo ${GIT_BRANCH} | tr  '[A-Z]' '[a-z]' | sed -e "s|#|-|g" | sed -e "s|origin\/||g" | sed -e "s|\_|-|g" | sed -e "s|\.|-|g")
#
#sed -i "s|[\$]{PDCI_APPLICATION_ENV}|tcti|g" docker-compose.yml
#sed -i "s|[\$]{PDCI_GIT_BRANCH}|${PDCI_GIT_BRANCH}|g" docker-compose.yml
#
#
#cat  docker-compose.yml
#
#exit 1



#set -ex

PDCI_GIT_BRANCH=$(echo ${GIT_BRANCH} | tr  '[A-Z]' '[a-z]' | sed -e "s|#|-|g" | sed -e "s|origin\/||g" | sed -e "s|\_|-|g" | sed -e "s|\.|-|g")

sed -i "s|[\$]{PDCI_APPLICATION_ENV}|tcti|g" docker-compose.yml
sed -i "s|[\$]{PDCI_GIT_BRANCH}|${PDCI_GIT_BRANCH}|g" docker-compose.yml

echo ""
echo ""
echo "GIT_BRANCH => ${GIT_BRANCH}"
echo ""
echo "GIT_LOCAL_BRANCH => ${GIT_LOCAL_BRANCH}"

echo "BUILD_NUMBER => ${BUILD_NUMBER}"
echo "BUILD_ID => ${BUILD_ID}"
echo "BUILD_DISPLAY_NAME => ${BUILD_DISPLAY_NAME}"

echo ""
echo ""
rm -rf .env
cp ./.env.docker.tcti .env

echo ""
echo "setando variaveis de usuario de conexao do banco de dados "
echo ""

__pdci_set_db_username_=$( export | grep '__pdci_set_db_username_' | sed 's|declare -x ||g' | sed 's|"||g')

for line in ${__pdci_set_db_username_} ;
do

#echo " >>> $line" ;

IFS='=' read -ra PARAMETROS <<< "$line"

 sed -i "s|${PARAMETROS[0]}|${PARAMETROS[1]}|g" ./.env

done

echo ""
echo ""
echo "setando variaveis senhas de banco pdci"
echo ""

__pdci_set_db_password_=$( export | grep '__pdci_set_db_password_' | sed 's|declare -x ||g' | sed 's|"||g')

for line in ${__pdci_set_db_password_} ;
do

#echo " >>> $line" ;

IFS='=' read -ra PARAMETROS <<< "$line"

#echo ""
#echo "${PARAMETROS[0]}, ${PARAMETROS[1]}, ${PARAMETROS[2]}"

 sed -i "s|${PARAMETROS[0]}|${PARAMETROS[1]}|g" ./.env

done


echo ""

#cat .env | while read line
#do
#    echo " export $line" >> export.env
#done
#export
echo ""
echo ""
#cat export.env
echo ""
echo ""
#source export.env
echo ""
__PDCI_DOCKER_HOST_TCTI=${pdci_docker_host_tcti}
__PDCI_APPLICATION_ENV=tcti


if [ -e docker-compose.yml ]; then
echo ""
	echo "Processando docker-compose.yml"
echo ""
else
  echo ""
  echo "ERRO:  Arquivo docker-compose.yml não existe"
  echo ""
  exit 1
fi;

if [ -e .env.docker.tcti ]; then
echo ""
	echo "Processando .env.docker.tcti"
echo ""
else
  echo ""
  echo " ERRO: Arquivo .env.docker.tcti não existe"
  echo ""
  exit 1
fi;

#add_rollback () {
#
#  if [ "${__last_tag_rollback}" == "" ]; then
#    echo ""
#    echo ""
#    echo "ERRO :: Favor informar a variavel __last_tag_rollback"
#    exit 145
#    echo ""
#    echo ""
#  fi
#  JENKINS_URL=http://127.0.0.1:8080
#  export TZ='America/Sao_Paulo'
#  data=$(date +%F_%H:%M:%S)
#  #IFS for jobs with spaces.
#  IFS=$(echo -en "\n\b")
#  SAVEIFS=$IFS
#  echo ${JENKINS_URL}
#  if [ ! -e jenkins-cli.jar ]; then
#  	wget --no-check-certificate  ${JENKINS_URL}/jnlpJars/jenkins-cli.jar
#  fi
#
#  rm -rf jenkins-cli.jar.*
#
##  export JENKINS_USER_ID=${__JENKINS_USER_ID}
##  export JENKINS_API_TOKEN=${__JENKINS_API_TOKEN}
#
#
#  __JOB_BASE_NAME_ROLLBACK=$(echo "${JOB_BASE_NAME}" | sed "s|-021-deploy-em|-022-rollback-previous-deploy-em|g" )
#
#  java -jar jenkins-cli.jar -s ${JENKINS_URL} get-job ${JOB_BASE_NAME} > ${__JOB_BASE_NAME_ROLLBACK}
#
#  sed -i "s|<defaultValue></defaultValue>||g"   ${__JOB_BASE_NAME_ROLLBACK}
#  sed -i "s|<sortMode>DESCENDING_SMART</sortMode>|<sortMode>DESCENDING_SMART</sortMode><defaultValue>${__last_tag_rollback}</defaultValue>|g"   ${__JOB_BASE_NAME_ROLLBACK}
#  sed -i "s|<name>\${__PDCI_BRANCH}</name>||g"   ${__JOB_BASE_NAME_ROLLBACK}
#  sed -i "s|<hudson.plugins.git.BranchSpec>|<hudson.plugins.git.BranchSpec><name>${__last_tag_rollback}</name>|g"   ${__JOB_BASE_NAME_ROLLBACK}
#
#  sed -i "s|<name>__PDCI_LISTA_IMAGENS_ROLLBACK</name>|<name>__PDCI_LISTA_IMAGENS_ROLLBACK</name><defaultValue>${__PDCI_LISTA_IMAGENS_ROLLBACK}</defaultValue>|g"   ${__JOB_BASE_NAME_ROLLBACK}
#
#  echo ""
#  echo "Criando job rollback-previous-deploy se não existir: ${__JOB_BASE_NAME_ROLLBACK}."
#  java -jar jenkins-cli.jar -s  ${JENKINS_URL} create-job ${__JOB_BASE_NAME_ROLLBACK} < ${__JOB_BASE_NAME_ROLLBACK} || true
#  echo "Atualizando job rollback-previous-deploy se existir: ${__JOB_BASE_NAME_ROLLBACK}."
#  java -jar jenkins-cli.jar -s  ${JENKINS_URL} update-job ${__JOB_BASE_NAME_ROLLBACK} < ${__JOB_BASE_NAME_ROLLBACK} || true
#}

#deploy_set_rollback () {
#
#echo ""
#echo ""
##echo "__registry_projeto_sem_tag => ${__registry_projeto_sem_tag}"
#echo ""
##echo "docker -H ${__PDCI_DOCKER_HOST_TCTI}  ps | grep ${__registry_projeto_sem_tag} | awk -F " " ' { print $2 }' | awk -F ":" ' { print $2 }'"
#echo ""
##strategy 1 => ja existe um container do servico rodando. Ira pegar a versao deste container e marcar para rollback
#
#__last_tag=$(docker -H ${__PDCI_DOCKER_HOST_TCTI}  ps | grep ${__registry_projeto_sem_tag} | awk -F " " ' { print $2 }' | awk -F ":" ' { print $2 }')
#
#if [ "${__last_tag}" != "" ]; then
#  echo ""
#  echo ""
#  echo "AVISO :: Marcado para executar a strategy 1"
#  echo ""
#  echo ""
#  __last_tag_rollback=$(echo "${__last_tag}" | sed "s|-|.|g" | sed "s|.rc|-rc|g" )
#  echo ""
#  echo ""
#else
#  echo ""
#  echo ""
#  echo "AVISO :: Não Foi possivel executar strategy 1 - Rollback do ultimo container no ar"
#  echo ""
#  echo "AVISO :: Passando para strategy 2 (RollBack Recursivo)"
#  echo ""
#  if [ "${__PDCI_LISTA_IMAGENS_ROLLBACK}" == "" ]; then
#    echo "      1) Criando lista de imagens de rollback."
#  	echo ""
#    docker  -H ${__PDCI_DOCKER_HOST_TCTI} images | grep -i rollback  | awk -F " " ' { print $2 }' > imagens_rollback.txt
#
#    __PDCI_LISTA_IMAGENS_ROLLBACK=$(cat imagens_rollback.txt | grep -v '^$' | paste -s -d"," -)
#  else
#  	echo "      1) Gerando lista de imagens de rollback."
#  	echo ""
#  	echo "${__PDCI_LISTA_IMAGENS_ROLLBACK}" | sed 's|,|\n|g' > imagens_rollback.txt
#    echo ""
#  fi
#
#  cat imagens_rollback.txt
#
#  echo "      2) Adicionando rollback versao"
#  __versao_rollback=$(head -n 1 imagens_rollback.txt)
#
#  echo "        2.1) versao marcada para rollback => ${__versao_rollback} "
#  __last_tag=$(echo "${__versao_rollback}" | sed "s|-rollback||g")
#  __last_tag_rollback=$(echo "${__last_tag}" | sed "s|-|.|g" | sed "s|.rc|-rc|g" )
#  echo "      3) Atualizando lista de rollback."
#  sed -i 1d imagens_rollback.txt
#  __PDCI_LISTA_IMAGENS_ROLLBACK=$(cat imagens_rollback.txt | grep -v '^$' | paste -s -d"," -)
#
#fi
#
#if [ "${__last_tag_rollback}" == "" ]; then
#
#echo "Aviso :: Nao existe ponto de restauracao para rollback. Provavelmente e o primeiro deploy do servico no ambiente"
#
#
#else
#
#echo " Desabilitado funcao de add_rollback"
##add_rollback
#
#fi
#
#
#
#}

deploy_service() {

  echo ""
  docker  -H ${__PDCI_DOCKER_HOST_TCTI} network create pdci_${__PDCI_APPLICATION_ENV}_network || true
  echo ""
  echo ""
  docker  -H ${__PDCI_DOCKER_HOST_TCTI} volume create pdci_${__PDCI_APPLICATION_ENV}_session_php || true
  echo ""
  echo ""
  docker  -H ${__PDCI_DOCKER_HOST_TCTI} volume create pdci_${__PDCI_APPLICATION_ENV}_data_${__pdci_nome_projeto} || true
  echo ""
  echo ""
  docker-compose  -H ${__PDCI_DOCKER_HOST_TCTI} --no-ansi -p pdci_${__PDCI_APPLICATION_ENV} -f  docker-compose.yml config
  echo ""
  echo ""
  docker-compose  -H ${__PDCI_DOCKER_HOST_TCTI} --no-ansi -p pdci_${__PDCI_APPLICATION_ENV} -f  docker-compose.yml pull
  echo ""
  echo "Removendo container ${__pdci_nome_projeto_mainapp} ..."
  echo ""
  docker-compose  -H ${__PDCI_DOCKER_HOST_TCTI} --no-ansi -p pdci_${__PDCI_APPLICATION_ENV} -f  docker-compose.yml rm -f -s ${__pdci_nome_projeto_mainapp}
  echo ""
  echo ""
  docker-compose  -H ${__PDCI_DOCKER_HOST_TCTI} --no-ansi -p pdci_${__PDCI_APPLICATION_ENV} -f docker-compose.yml up -d
  echo ""
  echo ""
  docker-compose  -H ${__PDCI_DOCKER_HOST_TCTI} --no-ansi -p pdci_${__PDCI_APPLICATION_ENV} -f docker-compose.yml ps

}

deploy_service_configurar () {
  #descobrir qual e a configurando arquivos de configuracao

  if [ -e src/br/gov/mainapp/ ]; then

  echo "Arquitetura mainapp!"

    if [ -e src/br/gov/mainapp/application/config ]; then

      echo "O código fonte e do proprio projeto MAINAPP!"

      cp src/br/gov/mainapp/application/config/config.ini mainapp_config.ini.pdci

    else

      echo "O codigo fonte e de um subprojeto do mainapp!"
      echo "Realizando download do arquivo de configuracao do MAINAPP para configurar o subprojeto"
      git clone https://gitlab+deploy-token-49731:Rww6qPNp4fLTs9X27AKf@gitlab.com/sisicmbio/app_mainapp.git


      cp app_mainapp/src/br/gov/mainapp/application/config/config.ini mainapp_config.ini.pdci

      rm -rf app_mainapp


    fi

    cp mainapp_config.ini.pdci mainapp_config.ini.pdci.bkp

    cp mainapp_config.ini.pdci mainapp_config.ini.erp

    sed -i "s|__pdci_set_db_password_pass_libcorp|<%= __pdci_set_db_password_pass_libcorp %>|g"  mainapp_config.ini.erp
    sed -i "s|__pdci_set_db_password_pass_libcorp|${__pdci_set_db_password_pass_libcorp}|g"  mainapp_config.ini.pdci


    sed -i "s|__pdci_set_db_password_pass_infoconv|<%= __pdci_set_db_password_pass_infoconv %>|g"  mainapp_config.ini.erp
    sed -i "s|__pdci_set_db_password_pass_infoconv|${__pdci_set_db_password_pass_infoconv}|g"  mainapp_config.ini.pdci

    sed -i "s|__pdci_set_db_password_pass_sicae|<%= __pdci_set_db_password_pass_sicae %>|g"  mainapp_config.ini.erp
    sed -i "s|__pdci_set_db_password_pass_sicae|${__pdci_set_db_password_pass_sicae}|g"  mainapp_config.ini.pdci

    sed -i "s|__pdci_set_db_password_pass_sisvp|<%= __pdci_set_db_password_pass_sisvp %>|g"  mainapp_config.ini.erp
    sed -i "s|__pdci_set_db_password_pass_sisvp|${__pdci_set_db_password_pass_sisvp}|g"  mainapp_config.ini.pdci

    sed -i "s|__pdci_set_db_password_pass_sismidia|<%= __pdci_set_db_password_pass_sismidia %>|g"  mainapp_config.ini.erp
    sed -i "s|__pdci_set_db_password_pass_sismidia|${__pdci_set_db_password_pass_sismidia}|g"  mainapp_config.ini.pdci

    sed -i "s|__pdci_set_db_password_pass_simac|<%= __pdci_set_db_password_pass_simac %>|g"  mainapp_config.ini.erp
    sed -i "s|__pdci_set_db_password_pass_simac|${__pdci_set_db_password_pass_simac}|g"  mainapp_config.ini.pdci

    sed -i "s|__pdci_set_db_password_pass_brigadista|<%= __pdci_set_db_password_pass_brigadista %>|g"  mainapp_config.ini.erp
    sed -i "s|__pdci_set_db_password_pass_brigadista|${__pdci_set_db_password_pass_brigadista}|g"  mainapp_config.ini.pdci

    sed -i "s|__pdci_set_db_password_pass_sisfamilias|<%= __pdci_set_db_password_pass_sisfamilias %>|g"  mainapp_config.ini.erp
    sed -i "s|__pdci_set_db_password_pass_sisfamilias|${__pdci_set_db_password_pass_sisfamilias}|g"  mainapp_config.ini.pdci

    sed -i "s|__pdci_set_db_password_pass_sit|<%= __pdci_set_db_password_pass_sit %>|g"  mainapp_config.ini.erp
    sed -i "s|__pdci_set_db_password_pass_sit|${__pdci_set_db_password_pass_sit}|g"  mainapp_config.ini.pdci

    sed -i "s|__pdci_set_db_password_pass_sgd|<%= __pdci_set_db_password_pass_sgd %>|g"  mainapp_config.ini.erp
    sed -i "s|__pdci_set_db_password_pass_sgd|${__pdci_set_db_password_pass_sgd}|g"  mainapp_config.ini.pdci

    sed -i "s|__pdci_set_db_password_pass_cairu|<%= __pdci_set_db_password_pass_cairu %>|g"  mainapp_config.ini.erp
    sed -i "s|__pdci_set_db_password_pass_cairu|${__pdci_set_db_password_pass_cairu}|g"  mainapp_config.ini.pdci

    sed -i "s|__pdci_set_db_password_pass_capacitacao|<%= __pdci_set_db_password_pass_capacitacao %>|g"  mainapp_config.ini.erp
    sed -i "s|__pdci_set_db_password_pass_capacitacao|${__pdci_set_db_password_pass_capacitacao}|g"  mainapp_config.ini.pdci

    sed -i "s|__pdci_set_db_password_pass_sofia|<%= __pdci_set_db_password_pass_sofia %>|g"  mainapp_config.ini.erp
    sed -i "s|__pdci_set_db_password_pass_sofia|${__pdci_set_db_password_pass_sofia}|g"  mainapp_config.ini.pdci

    sed -i "s|__pdci_set_db_password_pass_sisbio|<%= __pdci_set_db_password_pass_sisbio %>|g"  mainapp_config.ini.erp
    sed -i "s|__pdci_set_db_password_pass_sisbio|${__pdci_set_db_password_pass_sisbio}|g"  mainapp_config.ini.pdci

    sed -i "s|__pdci_set_db_password_pass_radius|<%= __pdci_set_db_password_pass_radius %>|g"  mainapp_config.ini.erp
    sed -i "s|__pdci_set_db_password_pass_radius|${__pdci_set_db_password_pass_radius}|g"  mainapp_config.ini.pdci

    sed -i "s|__pdci_set_ldap_password|<%= __pdci_set_ldap_password %>|g"  mainapp_config.ini.erp
    sed -i "s|__pdci_set_ldap_password|${__pdci_set_ldap_password}|g"  mainapp_config.ini.pdci

    sed -i "s|__pdci_set_db_password_pass_voluntariado|<%= __pdci_set_db_password_pass_voluntariado %>|g"  mainapp_config.ini.erp
    sed -i "s|__pdci_set_db_password_pass_voluntariado|${__pdci_set_db_password_pass_voluntariado}|g"  mainapp_config.ini.pdci

    sed -i "s|__pdci_set_db_password_pass_sigicmbio|<%= __pdci_set_db_password_pass_sigicmbio %>|g"  mainapp_config.ini.erp
    sed -i "s|__pdci_set_db_password_pass_sigicmbio|${__pdci_set_db_password_pass_sigicmbio}|g"  mainapp_config.ini.pdci

    sed -i "s|__pdci_set_db_password_pass_inventario|<%= __pdci_set_db_password_pass_inventario %>|g"  mainapp_config.ini.erp
    sed -i "s|__pdci_set_db_password_pass_inventario|${__pdci_set_db_password_pass_inventario}|g"  mainapp_config.ini.pdci

    sed -i "s|__pdci_set_db_password_pass_sisva|<%= __pdci_set_db_password_pass_sisva %>|g"  mainapp_config.ini.erp
    sed -i "s|__pdci_set_db_password_pass_sisva|${__pdci_set_db_password_pass_sisva}|g"  mainapp_config.ini.pdci


    container_id=$(docker -H ${__PDCI_DOCKER_HOST_TCTI}  ps | grep ${__registry_projeto} | awk -F " " ' { print $1 }')

    __path_mainapp_config_in_container=$(echo "/var/www/html/mainapp/br/gov/mainapp/application/config/config.ini")

    docker  -H ${__PDCI_DOCKER_HOST_TCTI}  cp ${container_id}:${__path_mainapp_config_in_container}  mainapp_config.ini.old
    docker  -H ${__PDCI_DOCKER_HOST_TCTI}  cp mainapp_config.ini.pdci ${container_id}:${__path_mainapp_config_in_container}
    docker  -H ${__PDCI_DOCKER_HOST_TCTI}  cp mainapp_config.ini.erp ${container_id}:${__path_mainapp_config_in_container}.erp
  #  docker  -H ${__PDCI_DOCKER_HOST_TCTI}  cp ${container_id}:${__path_mainapp_config_in_container}  mainapp_config.ini.new


    rm -f  mainapp_config.ini.pdci
   # rm -f  mainapp_config.ini.new

  else
      echo "NAO E Arquitetura mainapp!"
      echo ""
      echo "Testar se a arquitetura e do ZendFrameWork1"
      if [ -e src/application/configs/configs.ini ] && [ -e src/application/configs/database.ini ]; then

        echo ""
        echo ""

        echo "__path_config_database => ${__path_config_database} "
        echo "__db_password => ${__db_password} "

        cp src/application/configs/database.ini src/application/configs/database.ini.erp
        cp src/application/configs/configs.ini src/application/configs/configs.ini.erp

        cp src/application/configs/database.ini src/application/configs/database.ini.pdci
        cp src/application/configs/configs.ini src/application/configs/configs.ini.pdci


        sed -i "s|__pdci_set_ldap_password|<%= __pdci_set_ldap_password %>|g"  src/application/configs/configs.ini.erp
        sed -i "s|__pdci_set_db_password_pass_sicae|<%= __pdci_set_db_password_pass_sicae %>|g"  src/application/configs/database.ini.erp


        sed -i "s|__pdci_set_ldap_password|${__pdci_set_ldap_password}|g"  src/application/configs/configs.ini.pdci
        sed -i "s|__pdci_set_db_password_pass_sicae|${__pdci_set_db_password_pass_sicae}|g"  src/application/configs/database.ini.pdci



        container_id=$(docker -H ${__PDCI_DOCKER_HOST_TCTI}  ps | grep ${__registry_projeto} | awk -F " " ' { print $1 }')

        __path_config_database_in_container=$(echo "/var/www/html/${__pdci_nome_projeto_mainapp}/application/configs/database.ini")

        docker  -H ${__PDCI_DOCKER_HOST_TCTI}  cp ${container_id}:${__path_config_database_in_container} database.ini.old
        docker  -H ${__PDCI_DOCKER_HOST_TCTI}  cp src/application/configs/database.ini.pdci ${container_id}:${__path_config_database_in_container}
        docker  -H ${__PDCI_DOCKER_HOST_TCTI}  cp src/application/configs/database.ini.erp ${container_id}:${__path_config_database_in_container}.erp


        __path_config_ldap_in_container=$(echo "/var/www/html/${__pdci_nome_projeto_mainapp}/application/configs/configs.ini")


       docker  -H ${__PDCI_DOCKER_HOST_TCTI}  cp ${container_id}:${__path_config_ldap_in_container} configs.ini.old
       docker  -H ${__PDCI_DOCKER_HOST_TCTI}  cp src/application/configs/configs.ini.pdci ${container_id}:${__path_config_ldap_in_container}
       docker  -H ${__PDCI_DOCKER_HOST_TCTI}  cp src/application/configs/configs.ini.erp ${container_id}:${__path_config_ldap_in_container}.erp



        rm -f src/application/configs/database.ini.pdci
        rm -f src/application/configs/configs.ini.pdci

      else

        if [ -e src/requirements.txt ]; then

        	echo "Arquitetura python com Django"

        else

          echo "AVISO :: ARQUITETURA NAO MAPEADA PARA CONFIGURAR"
         # exit 1

        fi
      fi

  fi

  # FIM    descobrir qual e a configurando arquivos de configuracao
}


__pdci_name_group=$(echo ${PDCI_URL_GITLAB_PROJETO} | awk -F'/' '{printf $4}')
__pdci_nome_projeto=$(echo ${PDCI_URL_GITLAB_PROJETO} | awk -F'/' '{printf $5}'  | sed -e "s/.git//g" )
__pdci_nome_projeto_mainapp=$(echo ${PDCI_URL_GITLAB_PROJETO} | awk -F'/' '{printf $5}' | sed -e "s/.git//g" | sed -e "s/app_//g" )

__GIT_BRANCH=$(echo ${__PDCI_BRANCH} | tr  '[A-Z]' '[a-z]' | sed -e "s|#|-|g" | sed -e "s|origin\/||g" | sed -e "s|\_|-|g" | sed -e "s|\.|-|g")

echo "__GIT_BRANCH => ${__GIT_BRANCH}"

__registry_projeto=$(echo "${PDCI_REGISTRY}/${__pdci_name_group}/${__pdci_nome_projeto}:${__GIT_BRANCH}" )

__registry_projeto_sem_tag=$(echo "${PDCI_REGISTRY}/${__pdci_name_group}/${__pdci_nome_projeto}" )

#deploy_set_rollback

#echo "" >> .env
#echo "pdci_registry_${__pdci_nome_projeto_mainapp}=${__registry_projeto}" >> .env

echo "" >> .env
echo "PDCI_APP_VERSAO=${__GIT_BRANCH}.${BUILD_NUMBER}.(${GIT_COMMIT})" >> .env

docker  -H ${__PDCI_DOCKER_HOST_TCTI} logout

echo "${__pdci_registry_gitlab_deploy_token_password}" | docker  -H ${__PDCI_DOCKER_HOST_TCTI} login --username ${__pdci_registry_gitlab_deploy_token_username} --password-stdin  ${PDCI_REGISTRY}


#sed -i "s|[\$]{PDCI_APPLICATION_ENV}|${__PDCI_APPLICATION_ENV}|g" docker-compose.yml
#sed -i "s|[\$]{__pdci_nome_projeto}|${__pdci_nome_projeto}|g" docker-compose.yml


deploy_service

#deploy_service_configurar

###########################################################
#
#                   INICIO DE SCRIPT PARA PROVOCAR ERROR
#
#
#__registry_projeto_deploy=${__registry_projeto}-deploy${BUILD_NUMBER}
#docker -H ${__PDCI_DOCKER_HOST_TCTI} commit  ${container_id} ${__registry_projeto_deploy}

#rm -rf .env
#cp ./.env.docker.hmg .env
#echo "" >> .env
#echo "pdci_registry_${__pdci_nome_projeto_mainapp}=${__registry_projeto_deploy}" >> .env
#cat .env

#echo ""
#echo "Subindo imagem da Build do servico ${__pdci_nome_projeto_mainapp} ..."
#echo ""
#docker-compose  -H ${__PDCI_DOCKER_HOST_TCTI} -p pdci_${__PDCI_APPLICATION_ENV} -f  docker-compose.yml restart ${__pdci_nome_projeto_mainapp}

#docker -H ${__PDCI_DOCKER_HOST_TCTI} rmi $(docker -H ${__PDCI_DOCKER_HOST_TCTI} images | grep ${__registry_projeto_deploy} | awk -F " " ' { print $1 }')

#
#
#          FIM DE SCRIPT PARA PROVOCAR ERROR
#
########################################################################

rm -f export.env
echo ""
echo ""
cat .env
#rm -f .env
echo ""
echo ""
echo "docker  -H ${__PDCI_DOCKER_HOST_TCTI} image prune -f"
docker  -H ${__PDCI_DOCKER_HOST_TCTI} image prune -f
echo ""
echo ""
echo ""
## Verificar se os servicos subiram?
rm -f lista_servicos
echo ""
echo ""


sleep 5
verificar_status_container () {
docker-compose  -H ${__PDCI_DOCKER_HOST_TCTI} -p pdci_${__PDCI_APPLICATION_ENV} -f docker-compose.yml ps | awk -F " " ' { print $1 }' | sed "1d" | sed "1d" > lista_servicos
cat lista_servicos | while read line
do
	echo ""
    echo " Service => $line"
    echo ""

    container_id=$(docker -H ${__PDCI_DOCKER_HOST_TCTI}  ps | grep $line | awk -F " " ' { print $1 }')
    container_image_id=""
    container_image=""
    container_error=""
    container_logs=""
    if [ -n ${container_id} ]; then

    	container_image=$(docker -H ${__PDCI_DOCKER_HOST_TCTI}  ps -a | grep $line | awk -F " " ' { print $2 }')

        if [ "${container_id}" == "" ]; then
            echo ""
            echo "Lendo log do container"
        	container_log_id=$(docker -H ${__PDCI_DOCKER_HOST_TCTI}  ps -a | grep $line | awk -F " " ' { print $1 }')
        	container_logs=$(docker -H ${__PDCI_DOCKER_HOST_TCTI}  logs ${container_log_id})
            container_error=$(docker -H ${__PDCI_DOCKER_HOST_TCTI}  container inspect  --format='{{.State.Error}}' ${container_log_id})
			__nome_file_inspect=${container_log_id}_error_container_inspect
            docker -H ${__PDCI_DOCKER_HOST_TCTI}  container inspect  ${container_log_id} > ${__nome_file_inspect}
        else

          if [ "${container_image}" == "${__registry_projeto}" ]; then
            echo ""
            echo ""
            echo "DESABILITADO imagem de rollback"
           # echo "Rotular imagem de rollback"
           # docker  -H ${__PDCI_DOCKER_HOST_TCTI} tag ${__registry_projeto} ${__registry_projeto}-rollback
            echo ""
            echo ""

          fi

        fi

        echo "*****************************"
        echo "Setando limit de cpf e memoria"
        echo "   container_image => ${container_image}."
		echo ""

        case ${container_image} in
            *"app_sintax_ipt:develop"*)
                echo ""
                echo "--memory 5120m --memory-swap 5120m --cpus=\"16\""
                echo ""
                docker -H ${__PDCI_DOCKER_HOST_TCTI} update --memory 5120m --memory-swap 5120m --cpus="16"  ${container_id}
                ;;
            *"app_monitora/enketo:"*)
                echo ""
                echo "--memory 2048m --memory-swap 2048m --cpus=\"2\""
                echo ""
                docker -H ${__PDCI_DOCKER_HOST_TCTI} update --memory 2048m --memory-swap 2048m --cpus="2"  ${container_id}
                ;;
            *"app_monitora/monitora_ipt:"*)
                echo ""
                echo "--memory 5120m --memory-swap 5120m --cpus=\"16\""
                echo ""
                docker -H ${__PDCI_DOCKER_HOST_TCTI} update --memory 5120m --memory-swap 5120m --cpus="16"  ${container_id}
                ;;
             *)
                echo ""
                echo "--memory 1024m --memory-swap 1024m --cpus=\"2\""
                echo ""
                docker -H ${__PDCI_DOCKER_HOST_TCTI} update --memory 1024m --memory-swap 1024m --cpus="2"  ${container_id}

				deploy_service_configurar
                ;;
        esac

		echo ""
        echo "*****************************"
        echo ""
        echo ""


        echo "     container_id       => ${container_id}"
        echo "     container_image_id => ${container_image_id}"
        echo "     container_image    => ${container_image}"
        echo "     container_error    => ${container_error}"
        echo "     container_logs     => ${container_logs}"

    else
      echo "ERRO :: O conteiner do servico $line nao subiu. Verifique o problema"
      exit 1
    fi

    echo ""
done
}

verificar_status_container

rm -rf ./.env